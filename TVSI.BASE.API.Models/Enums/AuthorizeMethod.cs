﻿using System.ComponentModel;

namespace TVSI.BASE.API.Models.Enums
{
    public enum AuthorizeMethod
    {
        [Description("LDAP")]
        Ldap = 1,

        [Description("NORMAL")]
        Normal = 2
    }
}