﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TVSI.BASE.API.Models.Enums
{
    public enum Language
    {
        [Description("VI")]
        Vi = 1,
        [Description("EN")]
        En = 2
    }
}
