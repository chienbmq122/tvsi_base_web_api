﻿using System.ComponentModel;

namespace TVSI.BASE.API.Models.Enums
{
    public enum Channel
    {
        [Description("ALL")]
        All = 0,
        [Description("BACK")]
        Back = 1,
        [Description("ONLINE")]
        Online = 2
    }
}
