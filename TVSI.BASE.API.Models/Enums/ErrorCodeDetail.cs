﻿using System.ComponentModel;

namespace TVSI.BASE.API.Models.Enums
{
    //Get description from database
    public enum ErrorCodeDetail
    {
        Success = 0,        
        Exception = -99999,

        #region Code for Authen
        
        AccountHasBeenLocked = 69901,
        IncorrectInfoLogin = 69902,
        NotAllowedToAccessApplication = 69903,
        NotAllowedToAccessAction = 69904,
        TokenRequired = 69905,
        TokenNotFound = 69906,
        InvalidToken = 69907,
        NoDataFound = 69908,
        InvalidUserInfo = 69909,

        #endregion Code for Authen
    }
}