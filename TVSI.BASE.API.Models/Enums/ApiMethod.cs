﻿using System.ComponentModel;

namespace TVSI.BASE.API.Models.Enums
{
    public enum ApiMethod
    {
        [Description("POST")]
        POST = 1,

        [Description("GET")]
        GET = 2,

        [Description("PUT")]
        PUT = 3,

        [Description("DELETE")]
        DELETE = 4
    }
}