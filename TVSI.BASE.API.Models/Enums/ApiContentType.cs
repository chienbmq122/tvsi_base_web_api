﻿using System.ComponentModel;

namespace TVSI.BASE.API.Models.Enums
{
    public enum ApiContentType
    {
        [Description("application/json")]
        Json = 1,

        [Description("application/x-www-form-urlencoded")]
        FormUrlencoded = 2
    }
}
