﻿using System.ComponentModel;

namespace TVSI.BASE.API.Models.Enums
{
    public enum TokenType
    {
        [Description("ACCESS_TOKEN")]
        AccessToken = 1,

        [Description("REFRESH_TOKEN")]
        RefreshToken = 2
    }
}