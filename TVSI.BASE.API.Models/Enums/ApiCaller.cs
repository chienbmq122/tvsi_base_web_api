﻿namespace TVSI.BASE.API.Models.Enums
{
    public enum ApiCaller
    {
        HttpClient = 1,
        HttpWebRequest = 2,
        HttpWebClient = 3
    }
}