﻿namespace TVSI.BASE.API.Models.Dtos
{
    public class RoleDataDto
    {
        public string PermissionDataCode { get; set; }
        public string PermissionDataDesc { get; set; }
    }
}