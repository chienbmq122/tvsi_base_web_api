﻿namespace TVSI.BASE.API.Models.Model.Request;

public class RefreshTokenRequest
{
    [Required] 
    public string? RefreshToken { get; set; }
}