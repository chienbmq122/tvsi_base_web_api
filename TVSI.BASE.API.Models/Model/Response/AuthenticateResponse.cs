﻿namespace TVSI.BASE.API.Models.Model.Response
{
    public class AuthenticateResponse
    {
        public string? Username { get; set; }
        public string? Email { get; set; }
        public string? Phone { get; set; }
        public string? AccessToken { get; set; }
        public DateTime? ExpiresTime { get; set; }
        public string? RefreshToken { get; set; } 
        public DateTime? RefreshTokenExpiration { get; set; }
        //public List<MenuDto> Menus { get; set; }
        //public List<FunctionsDto> Functions { get; set; }
        public bool ShowCaptcha { get; set; } = false;
    }
}