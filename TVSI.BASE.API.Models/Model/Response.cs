﻿namespace TVSI.BASE.API.Models.Model
{
    public class Response<T>
    {
        public string Code { get; set; } = "-1";
        public string? Message { get; set; }
        public IEnumerable<T>? Data { get; set; } = null; 
    }
}
