﻿namespace TVSI.BASE.API.Models.Model;

public class Request<T> where T : class
{
    public T? ParaInfo { get; set; }
}