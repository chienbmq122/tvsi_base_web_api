
using Helper.Security;
using Microsoft.Extensions.Configuration;
using Serilog;
using TVSI.BASE.API.Models.Entities.Demo;
using TVSI_TVFM_API.Middlewares;

var env = Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT");

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;
var logging = builder.Logging;
var services = builder.Services;

#region AppSetting
configuration
    .AddJsonFile($"appsettings.json",false ,true)
    .AddJsonFile($"appsettings.{env}.json",true,true)
    .AddEnvironmentVariables();
#endregion AppSetting

#region Logging

#region Log4net

logging
    .SetMinimumLevel(LogLevel.Trace)
    .AddLog4Net("log4net.config");

#endregion Log4net

#region Serilog

#endregion Serilog   

//logging.ClearProviders();

//var serilogSetting = new ConfigurationBuilder()
//    .SetBasePath(Directory.GetCurrentDirectory())
//    .AddJsonFile(path: "serilogSetting.json", reloadOnChange: true, optional: false)
//    .Build();

//var logger = new LoggerConfiguration()
//    .MinimumLevel.Information()
//    .ReadFrom.Configuration(serilogSetting)
//    .WriteTo.Map("Name", "Other", (name, wt) => wt.File("./logs/" + DateTime.Now.ToString("yyyyMMdd") + "/" + name + ".log", rollingInterval: RollingInterval.Day))
//    .CreateLogger();

//logging.AddSerilog(logger);

#endregion Logging

// Add services to the container.
#region Detection

services.AddDetection();
services.AddDetectionCore().AddBrowser();
services.AddDetectionCore().AddPlatform();
services.AddDetectionCore().AddEngine();
services.AddDetectionCore().AddDevice();

//services.AddSession(options =>
//{
//    options.IdleTimeout = TimeSpan.FromSeconds(10);
//    options.Cookie.HttpOnly = true;
//    options.Cookie.IsEssential = true;
//});

#endregion Detection

#region Database

var demoConnStr = configuration.GetConnectionString("DemoConnection");
//Encry data
//var Encryconn = Crypto.Encrypt(demoConnStr, CommonConstants.EncryptionKeys);

//Decrypt data
var conn = Crypto.Decrypt(demoConnStr, CommonConstants.EncryptionKeys);

services.AddDbContext<DemoDbContext>(options => options.UseSqlServer(demoConnStr));

#endregion Database

#region Cache

//var EncryRedisPass = Crypto.Encrypt(configuration["Redis:Pwd"], CommonConstants.EncryptionKeys);

var redisPass = Crypto.Decrypt(configuration["Redis:Pwd"], CommonConstants.EncryptionKeys);
var redisConf = $"{configuration["Redis:Ip"]},password={redisPass}";
services.AddStackExchangeRedisCache(options => { options.Configuration = redisConf; });

var cm = ConnectionMultiplexer.Connect(redisConf);
services.AddSingleton<IConnectionMultiplexer>(cm);

#endregion Cache

#region ApiVersioning

services.AddApiVersioning(config =>
{
    config.DefaultApiVersion = new ApiVersion(1, 0);
    config.AssumeDefaultVersionWhenUnspecified = true;
    config.ReportApiVersions = true;
    config.ApiVersionReader = ApiVersionReader.Combine(
        new HeaderApiVersionReader("x-api-version"),
        new QueryStringApiVersionReader("v"));
});

services.AddVersionedApiExplorer(options =>
{
    options.GroupNameFormat = "'v'VVV";
    options.SubstituteApiVersionInUrl = true;
});

#endregion ApiVersioning

#region Authentication
services.AddAuthentication(option =>
{
    option.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    option.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(options =>
{
    options.TokenValidationParameters = new TokenValidationParameters
    {
        ValidateIssuer = true,
        ValidIssuer = configuration["Jwt:Issuer"],
        ValidateAudience = true,
        ValidAudience = configuration["Jwt:Audience"],
        ValidateIssuerSigningKey = true,
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(configuration["Jwt:Secret"])),
        RequireExpirationTime = false,
        ValidateLifetime = true,
        ClockSkew = TimeSpan.Zero
    };
});
#endregion Authentication

#region Swagger

services.AddEndpointsApiExplorer();
services.AddSwaggerGen(c =>
{
    c.OperationFilter<OperationFilter>();
    
    var xmlPath = Path.Combine(AppContext.BaseDirectory, "ApiDocument.xml");
    if(File.Exists(xmlPath))
        c.IncludeXmlComments(xmlPath);

    c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Name = "Authorization",
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer",
        BearerFormat = "JWT",
        In = ParameterLocation.Header,
        Description = "Add to request header with Authorization key and value format 'Bearer' [space] 'your jwt token'.\r\n\r\nExample: \"Bearer 24AB855E-CA1F-4098-8DCF-12F7AA9BCB82\""
    });

    c.AddSecurityRequirement(new OpenApiSecurityRequirement
    {
        {
            new OpenApiSecurityScheme
            {
                Reference = new OpenApiReference
                {
                    Type = ReferenceType.SecurityScheme,
                    Id = "Bearer"
                }
            },
            new string[] { }
        }
    });
});

#endregion Swagger


services.AddServices();

services.AddControllers().AddJsonOptions(options => options.JsonSerializerOptions.PropertyNamingPolicy = null);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment() || app.Environment.IsStaging())
{
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        var apiVersionDescriptions 
            = app.Services.GetService<IApiVersionDescriptionProvider>()?.ApiVersionDescriptions;
        if (apiVersionDescriptions == null) return;
        foreach (var description in apiVersionDescriptions)
            c.SwaggerEndpoint($"/swagger/{description.GroupName}/swagger.json",
                description.GroupName);
    });
}

app.UseHttpsRedirection();
app.UseRouting();

app.UseCors(x => x
    .AllowAnyOrigin()
    .AllowAnyMethod()
    .AllowAnyHeader());

app.UseHttpMetrics();

app.UseAuthorization();

app.UseMiddleware<ResponseMetricMiddleware>();
app.UseMiddleware<ErrorWrappingMiddleware>();
app.UseMiddleware<LoggingMiddleware>();

app.MapControllers();

app.Map("/metrics", metricsApp =>
{
    int.TryParse(configuration["Metrics:Port"], out var metricPort);
    var metricServer = new MetricServer(metricPort == 0 ? 9095 : metricPort);
    metricServer.Start();
});

app.Run();
