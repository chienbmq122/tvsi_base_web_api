﻿namespace TVSI.BASE.API.Middlewares.RequestExecutionTime
{
    public class RequestExecutionTimeMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<RequestExecutionTimeMiddleware> _logger;

        public RequestExecutionTimeMiddleware(RequestDelegate next, ILogger<RequestExecutionTimeMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public Task InvokeAsync(HttpContext context)
        {
            var watch = new Stopwatch();
            watch.Start();
            context.Response.OnStarting(() =>
            {
                watch.Stop();
                var responseTimeForCompleteRequest = watch.ElapsedMilliseconds;

                _logger.LogWarning("Request ended for {RequestPath} in {Duration} ms",
                    context.Request.Path, responseTimeForCompleteRequest);

                return Task.CompletedTask;
            });
            return this._next(context);
        }
    }
}