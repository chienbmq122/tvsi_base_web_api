﻿using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.IO;
using Newtonsoft.Json.Linq;

namespace TVSI_TVFM_API.Middlewares
{
    public class LoggingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger _logger;
        private readonly RecyclableMemoryStreamManager _recyclableMemoryStreamManager;


        public LoggingMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next;
            _logger = loggerFactory.CreateLogger<LoggingMiddleware>();
            _recyclableMemoryStreamManager = new RecyclableMemoryStreamManager();
        }


        public async Task Invoke(HttpContext context)
        {
            await LogRequest(context);
            await LogResponse(context);
        }


        private async Task LogRequest(HttpContext context)
        {
            context.Request.EnableBuffering();
            await using var requestStream = _recyclableMemoryStreamManager.GetStream();
            await context.Request.Body.CopyToAsync(requestStream);
            var bodyContent = ReadStream(requestStream);

            _logger.LogInformation($"Http Request Information: [env: {Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}]\n" +
                                   $"Schema:{context.Request.Scheme}\n " +
                                   $"Host: {context.Request.Host.Host}:{context.Request.Host.Port}\n " +
                                   $"Path: {context.Request.Path}\n " +
                                   $"QueryString: {context.Request.QueryString}\n " +
                                   $"Request Body: {bodyContent}");
            context.Request.Body.Position = 0;
        }


        private static string ReadStream(Stream stream)
        {
            const int bufferLength = 4096;
            stream.Seek(0, SeekOrigin.Begin);
            using var textWriter = new StringWriter();
            using var reader = new StreamReader(stream);
            var buffer = new char[bufferLength];
            int readCount;
            do
            {
                readCount = reader.ReadBlock(buffer, 0, bufferLength);
                textWriter.Write(buffer, 0, readCount); 
            } while (readCount > 0);

            var body = textWriter.ToString();
            return !body.IsJsonString() ? body : body.WriteLogNonePassword().ToString();
        }


        private async Task LogResponse(HttpContext context)
        {
            var originalBodyStream = context.Response.Body;
            await using var responseBody = _recyclableMemoryStreamManager.GetStream();
            context.Response.Body = responseBody;
            await _next(context);
            context.Response.Body.Seek(0, SeekOrigin.Begin);
            var bodyContent = await new StreamReader(context.Response.Body).ReadToEndAsync();
            context.Response.Body.Seek(0, SeekOrigin.Begin);

            _logger.LogInformation($"Http Response Information: [env: {Environment.GetEnvironmentVariable("ASPNETCORE_ENVIRONMENT")}]\n" +
                                   $"Schema:{context.Request.Scheme} \n" +
                                   $"Host: {context.Request.Host.Host}:{context.Request.Host.Port}\n " +
                                   $"Path: {context.Request.Path}\n " +
                                   $"QueryString: {context.Request.QueryString}\n " +
                                   $"Response Body: {bodyContent}");
            await responseBody.CopyToAsync(originalBodyStream); 
        }
    }
}
