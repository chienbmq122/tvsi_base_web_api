﻿using System.Net;
using TVSI.BASE.API.Models.Enums;

namespace TVSI.BASE.API.Middlewares
{
    public class ErrorWrappingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ErrorWrappingMiddleware> _logger;

        public ErrorWrappingMiddleware(RequestDelegate next, ILogger<ErrorWrappingMiddleware> logger)
        {
            _next = next;
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));
        }

        public async Task Invoke(HttpContext context)
        {
                await _next.Invoke(context);

            if (context.Response.StatusCode is (int)HttpStatusCode.NotFound or (int)HttpStatusCode.MethodNotAllowed)
            {
                context.Response.ContentType = "application/json";
                var response = new Response<string>()
                {
                    Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                    Message = "wrong request format!",
                    Data = null
                };

                var json = JsonConvert.SerializeObject(response);

                await context.Response.WriteAsync(json);
            }
        }
    }
}
