﻿namespace TVSI.BASE.API.Middlewares.Prometheus
{
    public class MetricReporter
    {
        private readonly ILogger<MetricReporter> _logger;
        private readonly Counter _requestCounter;
        private readonly Histogram _responseTimeHistogram;
        private readonly Counter _requestCmd;

        public MetricReporter(ILogger<MetricReporter> logger)
        {
            _logger = logger ?? throw new ArgumentNullException(nameof(logger));

            _requestCounter = Metrics
                .CreateCounter("requests_total", "The total number of requests serviced by this API.");

            _responseTimeHistogram = Metrics
                .CreateHistogram("request_duration_seconds", "The duration in seconds between the response to a request.",
                    new HistogramConfiguration
                    {
                        Buckets = Histogram.ExponentialBuckets(0.01, 2, 10),
                        LabelNames = new[] { "status_code", "method" }
                    });

            _requestCmd = Metrics
                .CreateCounter("gify_biz_type_requests_total", "The total number of requests going through the GIFY api gateway.", new CounterConfiguration()
                {
                    LabelNames = new[] { "method", "endpoint", "biz" }
                });
        }

        public void RegisterRequest()
        {
            _requestCounter.Inc();
        }

        public void RegisterResponseTime(int statusCode, string method, TimeSpan elapsed)
        {
            _responseTimeHistogram.WithLabels(statusCode.ToString(), method).Observe(elapsed.TotalSeconds);
        }

        public void RegisterRequestWithCmd(HttpContext context)
        {
            if (context.Request.Path == "/api/Gateway/GatewayApi")
            {
                var syncIoFeature = context.Features.Get<IHttpBodyControlFeature>();
                if (syncIoFeature != null)
                {
                    syncIoFeature.AllowSynchronousIO = true;
                }

                var req = context.Request;
                req.EnableBuffering();

                var body = req.BodyReader.AsStream(true);

                string content;
                using (var reader = new StreamReader(body, Encoding.UTF8))
                {
                    content = reader.ReadToEnd();
                    req.Body.Position = 0;
                }

                //var cmd = JsonConvert.DeserializeObject<Request>(content);
                //_requestCmd.WithLabels(context.Request.Method, context.Request.Path, cmd.BizType).Inc();
            }
        }
    }
}