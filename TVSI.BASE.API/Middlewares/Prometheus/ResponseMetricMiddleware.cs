﻿namespace TVSI.BASE.API.Middlewares.Prometheus
{
    public class ResponseMetricMiddleware
    {
        private readonly RequestDelegate _request;
        private readonly ILogger<ResponseMetricMiddleware> _logger;

        public ResponseMetricMiddleware(RequestDelegate request, ILogger<ResponseMetricMiddleware> logger)
        {
            _request = request ?? throw new ArgumentNullException(nameof(request));
            _logger = logger;
        }

        public async Task Invoke(HttpContext httpContext, MetricReporter reporter)
        {
            var path = httpContext.Request.Path.Value;
            if (path == "/metrics")
            {
                await _request.Invoke(httpContext);
                return;
            }
            var sw = Stopwatch.StartNew();

            try
            {
                reporter.RegisterRequestWithCmd(httpContext);
                await _request.Invoke(httpContext);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            }
            finally
            {
                sw.Stop();
                reporter.RegisterRequest();
                reporter.RegisterResponseTime(httpContext.Response.StatusCode, httpContext.Request.Method, sw.Elapsed);
                _logger.LogWarning($"Request ended for {httpContext.Request.Path} in {sw.Elapsed} ms");
            }
        }
    }
}