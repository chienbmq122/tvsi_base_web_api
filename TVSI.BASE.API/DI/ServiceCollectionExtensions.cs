﻿namespace TVSI.BASE.API.DI
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddServices(this IServiceCollection services)
        {
            services.AddTransient<IConfigureOptions<SwaggerGenOptions>, SwaggerOptions>();
            services.AddSingleton<MetricReporter>();
            services.AddScoped<IApiCallerService, ApiCallerService>();
            services.AddScoped<IAuthService, AuthService>();
            services.AddScoped<IRedisService, RedisService>();
            services.AddScoped<IFileService, FileService>();
            services.AddScoped<IDistributeCacheService, DistributeCacheService>();
            services.AddScoped<IEmailService, EmailService>();

            return services;
        }
    }
}