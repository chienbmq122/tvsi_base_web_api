﻿namespace TVSI.BASE.API.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public abstract class BaseController<T> : ControllerBase where T : class
    {
        protected readonly IConfiguration _config;
        protected readonly IDetectionService _detection;
        protected readonly ILogger<T> _logger;

        protected BaseController(ILogger<T> logger, IConfiguration config, IDetectionService detection)
        {
            _logger = logger;
            _config = config;
            _detection = detection;
        }

        #region helper methods

        protected bool CheckClaimInfo(bool onlyDevice)
        {
            if (!onlyDevice) return true;

            var detectionToken = new DetectionInfo
            {
                Browser = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Browser")?.Value,
                ClientIp = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "ClientIp")?.Value,
                Device = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Device")?.Value,
                Engine = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Engine")?.Value,
                Platform = HttpContext.User.Claims.FirstOrDefault(c => c.Type == "Platform")?.Value
            };

            return CheckClientInfo(GetDetectionInfo(), detectionToken);
        }

        protected DetectionInfo GetDetectionInfo()
        {
            var device = _detection.Device.Type switch
            {
                Device.Desktop => "Desktop",
                Device.Car => "Car",
                Device.Console => "Console",
                Device.IoT => "IoT",
                Device.Mobile => "Mobile",
                Device.Tablet => "Tablet",
                Device.Tv => "Tv",
                Device.Watch => "Watch",
                _ => "Unknown"
            };

            return new DetectionInfo
            {
                Browser = $"{_detection.Browser.Name}{_detection.Browser.Version}",
                Platform = $"{_detection.Platform.Name}{_detection.Platform.Version}{_detection.Platform.Processor}",
                Engine = _detection.Engine.Name.ToString(),
                Device = device,
                ClientIp = GetIpAddress()
            };
        }

        protected bool CheckClientInfo(DetectionInfo detectionClient, DetectionInfo detectionToken)
        {
            return detectionClient.CompareModel(detectionToken);
        }

        protected string GetIpAddress()
        {
            var ipAddress = string.Empty;
            try
            {
                if (Request.Headers.ContainsKey("True-Client-IP") &&
                    !string.IsNullOrEmpty(Request.Headers["True-Client-IP"]))
                    ipAddress = Request.Headers["True-Client-IP"]; //if the user is behind a proxy server
                if (ipAddress == string.Empty)
                    if (Request.Headers.ContainsKey("CF-CONNECTING-IP") &&
                        !string.IsNullOrEmpty(Request.Headers["CF-CONNECTING-IP"]))
                        ipAddress = Request.Headers["CF-CONNECTING-IP"];
                if (ipAddress == string.Empty)
                    if (Request.Headers.ContainsKey("X-Forwarded-For") &&
                        !string.IsNullOrEmpty(Request.Headers["X-Forwarded-For"]))
                        ipAddress = Request.Headers["X-Forwarded-For"];
                if (ipAddress == string.Empty)
                    ipAddress = HttpContext.Connection.RemoteIpAddress?.MapToIPv4().ToString();
            }
            catch (Exception ex)
            {
                _logger.LogError(
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            }

            return ipAddress;
        }

        #endregion helper methods
    }
}