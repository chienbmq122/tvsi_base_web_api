﻿using Microsoft.AspNetCore.Mvc;

namespace TVSI_TVFM_API.Controllers.v1._1
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class BaseController : Controller
    {
        protected readonly IConfiguration _config;
        protected readonly IDetectionService _detection;
        protected readonly Serilog.ILogger _logger;

        protected BaseController(Serilog.ILogger logger, IConfiguration config, IDetectionService detection)
        {
            _logger = logger;
            var username = HttpContext.User.Claims.FirstOrDefault(c =>
                c.Type == "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/nameidentifier")?.Value;
            _logger = _logger.ForContext("Name", username == null ? "lammn" : username);

            _config = config;
            _detection = detection;
        }
    }
}
