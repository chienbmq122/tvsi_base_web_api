﻿namespace TVSI.BASE.API.Services.Interfaces
{
    public interface IDistributeCacheService
    {
        //define methods for load data from database and save to cache
        Task LoadDataToCache();
        string? GetErrCodeMessage(int errCode, string channel, string lang);
    }
}