﻿namespace TVSI.BASE.API.Services.Interfaces
{
    public interface IApiCallerService
    {
        /// <summary>
        /// Call Api uri
        /// </summary>
        /// <param name="url"></param>
        /// <param name="postData"></param>
        /// <param name="method"></param>
        /// <param name="contentType"></param>
        /// <param name="apiCaller"></param>
        /// <returns></returns>
        Task<Tuple<int, string, string>?> ApiCaller(string url, string postData, ApiMethod method,
            ApiContentType contentType = ApiContentType.Json, ApiCaller apiCaller = Models.Enums.ApiCaller.HttpClient);
    }
}