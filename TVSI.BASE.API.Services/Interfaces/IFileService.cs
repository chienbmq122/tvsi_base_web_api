﻿namespace TVSI.BASE.API.Services.Interfaces
{
    public interface IFileService
    {
        /// <summary>
        /// Export Data to file using predefined template
        /// </summary>
        /// <param name="fileExportPath"></param>
        /// <param name="templateFilePath"></param>
        /// <param name="data"></param>
        /// <param name="deliminator"></param>
        /// <param name="fileType"></param>
        void ExportDataToFileTemplate(string fileExportPath, string templateFilePath, DataSet data, string[] deliminator = null!, FileType fileType = FileType.Excel);

        /// <summary>
        /// Export error message to file
        /// </summary>
        /// <param name="fileExportPath"></param>
        /// <param name="noti"></param>
        /// <param name="fileType"></param>
        void ExportErrorMessageToFile(string fileExportPath, object noti, FileType fileType = FileType.Excel);
        
        /// <summary>
        /// Deleting multiple files with wildcard
        /// </summary>
        /// <param name="directoryPath"></param>
        /// <param name="wildcard"></param>
        void DeleteFile(string directoryPath, string wildcard);
    }
}