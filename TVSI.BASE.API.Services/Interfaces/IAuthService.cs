﻿using TVSI.BASE.API.Models.Dtos;
using TVSI.BASE.API.Models.Model.Response;

namespace TVSI.BASE.API.Services.Interfaces
{
    public interface IAuthService
    {
        Task<Response<AuthenticateResponse>> AuthenticateAsync(AuthenticateRequest model, DetectionInfo detectInfo);

        Response<bool> Logout(string accessToken, string Username);

        Response<bool> LockLogin(string Username, bool source, int loginFailedCount = -1);

        Response<bool> UnlockLogin(string Username);

        bool VerifyTokenBlackList(string accessToken);

        bool VerifyTokenDevice(string accessToken, string? Username);

        Response<AuthenticateResponse> RefreshToken(string token, DetectionInfo detectionInfo);

        bool RevokeRefreshToken(string token, string ipAddress);

        Response<RefreshTokenDto> GetRefreshTokenByUsername(string Username);

        Response<UserDto> GetAll();

        UserDto GetUserByUsername(string Username);

        UserRoleDto GetFullUserInfo(string Username);

        bool CheckLoginLdap(string userDomain, string password);

        bool CheckUserPermission(string bizType, string Username, ref string funcCode, ref string actionCode, ref string rs, ref string rsMsg);
    }
}