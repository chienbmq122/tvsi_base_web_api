﻿using TVSI.BASE.API.Models.Entities.Demo;

namespace TVSI.BASE.API.Services
{
    public abstract class BaseService<T> where T : class
    {
        protected readonly ILogger<T> _logger;
        protected readonly IConfiguration _config;
        protected readonly DemoDbContext _dbContext;

        protected BaseService(ILogger<T> logger, IConfiguration config, DemoDbContext dbContext)
        {
            _logger = logger;
            _config = config;
            _dbContext = dbContext;
        }
    }
}