﻿using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using Novell.Directory.Ldap;
using TVSI.BASE.API.Models.Dtos;
using TVSI.BASE.API.Models.Entities.Demo;
using TVSI.BASE.API.Models.Model.Response;

namespace TVSI.BASE.API.Services.Impls;

public class AuthService : BaseService<AuthService>, IAuthService
{
    private readonly int _accessFailedCount;
    private readonly IApiCallerService _apiCaller;
    private readonly IDistributeCacheService _distributeCache;
    private readonly string _domain;
    private readonly bool _onlyDevice;
    private readonly IRedisService _redisCache;
    private readonly int _showCaptcha;

    private readonly int _sqlTimeout;
    private readonly int _userInfoTimeout;
    private readonly int _userLockedTimeout;
    private readonly string _domainName;
    private readonly bool _ldapIsSSL;

    private readonly string _ldapServers;

    public AuthService(IRedisService redisCache, IApiCallerService apiCaller,
        IDistributeCacheService distributeCache,
        ILogger<AuthService> logger, DemoDbContext dbContext, IConfiguration config)
        : base(logger, config, dbContext)
    {
        _redisCache = redisCache;
        _apiCaller = apiCaller;
        _distributeCache = distributeCache;

        _sqlTimeout = _config["Timeout:Database"] == null
            ? CommonConstants.SqlServerTimeout
            : int.Parse(_config["Timeout:Database"]);

        _userInfoTimeout = (_config["Timeout:Cache"] == null
            ? CommonConstants._1Hours
            : int.Parse(_config["Timeout:Cache"])) * 60; //seconds

        _accessFailedCount = _config["Login:AccessFailedCount"] == null
            ? CommonConstants.AccessFailedCount
            : int.Parse(_config["Login:AccessFailedCount"]);

        _showCaptcha = _config["Login:ShowCaptcha"] == null
            ? CommonConstants.ShowCaptcha
            : int.Parse(_config["Login:ShowCaptcha"]);

        _userLockedTimeout = (_config["Login:LockedTime"] == null
            ? CommonConstants._1Hours
            : int.Parse(_config["Login:LockedTime"])) * 60; //seconds

        _onlyDevice = _config["Login:OnlyDevice"] == null || bool.Parse(_config["Login:OnlyDevice"]);

        _ldapServers = _config["Ldap:Host"];

        _ldapIsSSL = _config["Ldap:IsSSL"] == null || bool.Parse(_config["Ldap:IsSSL"]);

        _domainName = _config["Ldap:DomainName"] == null
            ? CommonConstants.DomainName
            : _config["Ldap:DomainName"];
    }

    public async Task<Response<AuthenticateResponse>> AuthenticateAsync(AuthenticateRequest model,
        DetectionInfo detectInfo)
    {
        try
        {
            var keyAccessFailedCount = CacheKeys.AccessFailedCount + model.Username;

            //Get num login failed
            var loginFailedCount = _redisCache.GetKey<int>(keyAccessFailedCount);
            if (loginFailedCount >= _accessFailedCount)
                return new Response<AuthenticateResponse>
                {
                    Code = ((int)ErrorCodeDetail.AccountHasBeenLocked).ErrorCodeFormat(),
                    Message = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.AccountHasBeenLocked,
                        Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription()),
                    Data = null
                };

            var userInfo = CheckLoginByUsername(model.Username, model.Password, out var refCode, out var refMesg);
            if (userInfo == null)
            {
                loginFailedCount += 1;
                LockLogin(model.Username, false, loginFailedCount);

                return new Response<AuthenticateResponse>
                {
                    Code = refCode,
                    Message = refMesg,
                    Data = new List<AuthenticateResponse>
                    {
                        new()
                        {
                            Username = model.Username,
                            ShowCaptcha = loginFailedCount >= _showCaptcha
                        }
                    }
                };
            }

            _redisCache.RemoveKey(keyAccessFailedCount);

            //save to cache
            var userInfoKey = CacheKeys.UserInfo + userInfo.Username;
            _redisCache.SetKey(userInfoKey, userInfo, _userInfoTimeout);

            //get token
            var expiresTime = DateTime.UtcNow.AddSeconds(-1);
            var accessToken = GenerateAccessToken(userInfo, detectInfo, ref expiresTime);

            var refresh = _dbContext.RefreshTokens.FirstOrDefault(x =>
                x.Username == userInfo.Username && x.Expires > DateTime.Now &&
                (x.RevokeDate == null || x.RevokeDate > DateTime.Now));

            RefreshToken refreshToken;
            if (refresh == null)
            {
                refreshToken = GenerateRefreshToken(userInfo.Username, detectInfo.ClientIp);
                _dbContext.RefreshTokens.Add(refreshToken);
            }
            else
            {
                refreshToken = refresh;
            }

            var logLogin = BuildLogLogin(userInfo.Username, Channel.Back.ToEnumDescription(), detectInfo);
            _dbContext.LogLogins.Add(logLogin);
            await _dbContext.SaveChangesAsync();

            return new Response<AuthenticateResponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.Success,
                    Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription()),
                Data = new List<AuthenticateResponse>
                {
                    new()
                    {
                        Username = userInfo.Username,
                        Email = userInfo.Email,
                        Phone = userInfo.Phone,
                        AccessToken = accessToken,
                        ExpiresTime = expiresTime,
                        RefreshToken = refreshToken.Token,
                        RefreshTokenExpiration = refreshToken.Expires
                    }
                }
            };
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<AuthenticateResponse>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.Exception,
                    Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription()),
                Data = null
            };
        }
    }

    public Response<bool> Logout(string accessToken, string Username)
    {
        try
        {
            var result = RevokeToken(accessToken);

            //remove userInfo
            var userInfoKey = CacheKeys.UserInfo + Username;
            var userInfoRemove = _redisCache.RemoveKey(userInfoKey);

            return new Response<bool>
            {
                Code = result ? "1" : "0",
                Message = result ? "" : "Logout failed.",
                Data = null
            };
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.Exception,
                    Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription()),
                Data = null
            };
        }
    }

    // source 0: login, 1: api
    public Response<bool> LockLogin(string Username, bool source, int loginFailedCount = -1)
    {
        try
        {
            loginFailedCount = loginFailedCount == -1 ? _accessFailedCount : loginFailedCount;
            if (source)
            {
                var userInfo = GetUserByUsername(Username);
                if (userInfo == null)
                    return new Response<bool>
                    {
                        Code = ((int)ErrorCodeDetail.InvalidUserInfo).ErrorCodeFormat(),
                        Message = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.InvalidUserInfo,
                            Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription()),
                        Data = null
                    };
            }

            var key = CacheKeys.AccessFailedCount + Username;
            var result = _redisCache.SetKey(key, loginFailedCount, _userLockedTimeout);


            return new Response<bool>
            {
                Code = result ? "1" : "0",
                Message = result ? "" : "Lock login failed.",
                Data = null
            };
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.Exception,
                    Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription()),
                Data = null
            };
        }
    }

    public Response<bool> UnlockLogin(string Username)
    {
        try
        {
            var userInfo = GetUserByUsername(Username);
            if (userInfo == null)
                return new Response<bool>
                {
                    Code = ((int)ErrorCodeDetail.InvalidUserInfo).ErrorCodeFormat(),
                    Message = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.InvalidUserInfo,
                        Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription()),
                    Data = null
                };

            var key = CacheKeys.AccessFailedCount + Username;
            var result = _redisCache.RemoveKey(key);

            return new Response<bool>
            {
                Code = result ? "1" : "0",
                Message = result ? "" : "Unlock login failed.",
                Data = null
            };
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<bool>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.Exception,
                    Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription()),
                Data = null
            };
        }
    }

    public bool VerifyTokenBlackList(string accessToken)
    {
        try
        {
            var key = CacheKeys.TokenRevoked + accessToken;
            var tokenRevoked = _redisCache.GetKey<bool>(key);

            return tokenRevoked;
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return true;
        }
    }

    public bool VerifyTokenDevice(string accessToken, string? Username)
    {
        try
        {
            var tokenDevice = false;

            if (_onlyDevice)
            {
                var key = CacheKeys.TokenCurrDevice + Username + accessToken;
                tokenDevice = _redisCache.GetKey<bool>(key);
            }
            else
            {
                tokenDevice = true;
            }

            return tokenDevice;
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return false;
        }
    }

    public Response<AuthenticateResponse> RefreshToken(string token, DetectionInfo detectInfo)
    {
        try
        {
            var refreshToken = _dbContext.RefreshTokens.FirstOrDefault(x => x.Token == token
                                                                            && x.Expires > DateTime.Now
                                                                            && (x.RevokeDate == null ||
                                                                                x.RevokeDate > DateTime.Now));
            if (refreshToken == null)
                return new Response<AuthenticateResponse>
                {
                    Code = ((int)ErrorCodeDetail.InvalidToken).ErrorCodeFormat(),
                    Message = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.InvalidToken,
                        Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription()),
                    Data = null
                };

            var user = _dbContext.Users.FirstOrDefault(u => u.Username == refreshToken.Username);
            if (user == null)
                return new Response<AuthenticateResponse>
                {
                    Code = ((int)ErrorCodeDetail.NoDataFound).ErrorCodeFormat(),
                    Message = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.NoDataFound,
                        Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription()),
                    Data = null
                };

            var userInfo = GetFullUserInfo(user.Username);
            if (userInfo == null)
                return new Response<AuthenticateResponse>
                {
                    Code = ((int)ErrorCodeDetail.NoDataFound).ErrorCodeFormat(),
                    Message = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.NoDataFound,
                        Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription()),
                    Data = null
                };

            // replace old refresh token with a new one and save
            var newRefreshToken = GenerateRefreshToken(refreshToken.Username, detectInfo.ClientIp);
            refreshToken.RevokeDate = DateTime.Now;
            refreshToken.RevokedByIp = detectInfo.ClientIp;
            refreshToken.ReplacedByToken = newRefreshToken.Token;

            _dbContext.Add(newRefreshToken);
            _dbContext.SaveChanges();

            // generate new jwt
            var expiresTime = DateTime.UtcNow.AddSeconds(-1);
            var accessToken = GenerateAccessToken(userInfo, detectInfo, ref expiresTime);

            return new Response<AuthenticateResponse>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.Success,
                    Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription()),
                Data = new List<AuthenticateResponse>
                {
                    new()
                    {
                        Username = user.Username,
                        Email = user.Email,
                        Phone = user.Phone,
                        AccessToken = accessToken,
                        ExpiresTime = expiresTime,
                        RefreshToken = newRefreshToken.Token,
                        RefreshTokenExpiration = newRefreshToken.Expires
                    }
                }
            };
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<AuthenticateResponse>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.Exception,
                    Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription()),
                Data = null
            };
        }
    }

    public bool RevokeRefreshToken(string token, string ipAddress)
    {
        try
        {
            var refreshToken = _dbContext.RefreshTokens.FirstOrDefault(x => x.Token == token
                                                                            && x.Expires > DateTime.Now
                                                                            && (x.RevokeDate == null ||
                                                                                x.RevokeDate > DateTime.Now));

            // return false if no user found with token
            if (refreshToken == null) return false;

            // revoke token and save
            refreshToken.RevokeDate = DateTime.Now;
            refreshToken.RevokedByIp = ipAddress;

            _dbContext.Update(refreshToken);
            _dbContext.SaveChanges();

            return true;
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return false;
        }
    }

    public Response<RefreshTokenDto> GetRefreshTokenByUsername(string Username)
    {
        try
        {
            var su = _dbContext.Users.Find(Username);
            if (su == null)
                return new Response<RefreshTokenDto>
                {
                    Code = ((int)ErrorCodeDetail.NoDataFound).ErrorCodeFormat(),
                    Message = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.NoDataFound,
                        Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription()),
                    Data = null
                };

            var sr = _dbContext.RefreshTokens.Where(m => m.Username == Username);

            return new Response<RefreshTokenDto>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.Success,
                    Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription()),
                Data = new List<RefreshTokenDto>
                {
                    new()
                    {
                        Username = su.Username,
                        Email = su.Email,
                        Phone = su.Phone,
                        RefreshToken = sr.ToList()
                    }
                }
            };
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<RefreshTokenDto>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.Exception,
                    Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription()),
                Data = null
            };
        }
    }

    public Response<UserDto> GetAll()
    {
        try
        {
            var users = _dbContext.Users.Select(m => new UserDto
            {
                Username = m.Username,
                Email = m.Email,
                Phone = m.Phone
            }).ToList();

            return new Response<UserDto>
            {
                Code = ((int)ErrorCodeDetail.Success).ErrorCodeFormat(),
                Message = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.Success,
                    Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription()),
                Data = users
            };
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return new Response<UserDto>
            {
                Code = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat(),
                Message = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.Exception,
                    Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription()),
                Data = null
            };
        }
    }

    public UserDto? GetUserByUsername(string Username)
    {
        try
        {
            var user = _dbContext.Users.FirstOrDefault(m => m.Username == Username);
            return new UserDto
            {
                Username = user.Username,
                Email = user.Email,
                Phone = user.Phone
            };
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return null;
        }
    }

    public UserRoleDto GetFullUserInfo(string Username)
    {
        try
        {
            var info = _dbContext.Users.FirstOrDefault(m => m.Username == Username);

            if (info == null)
                return null;

            return new UserRoleDto
            {
                Username = info.Username,
                Phone = info.Phone,
                Email = info.Email
            };
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return null;
        }
    }

    public bool CheckLoginLdap(string userDomain, string password)
    {
        var userDn = $"{userDomain}@{_domainName}";
        try
        {
            using (var connection = new LdapConnection { SecureSocketLayer = false })
            {
                connection.Connect(_ldapServers,
                    _ldapIsSSL ? LdapConnection.DefaultSslPort : LdapConnection.DefaultPort);
                connection.Bind(userDn, password);
                if (connection.Bound)
                    return true;
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
        }

        return false;
    }


    public bool CheckUserPermission(string bizType, string Username, ref string funcCode, ref string actionCode,
        ref string rs, ref string rsMsg)
    {
        try
        {
            var userInfoKey = CacheKeys.UserInfo + Username;
            var userRoleInfo = _redisCache.GetKey<UserRoleDto>(userInfoKey);

            rs = "";
            rsMsg = "";
            return true;
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            rs = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat();
            rsMsg = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.Exception, Channel.Back.ToEnumDescription(),
                Language.Vi.ToEnumDescription());
            return false;
            ;
        }
    }

    private UserRoleDto CheckLoginByUsername(string Username, string pass, out string refCode, out string refMesg)
    {
        refCode = ((int)ErrorCodeDetail.IncorrectInfoLogin).ErrorCodeFormat();
        refMesg = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.IncorrectInfoLogin,
            Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription());
        try
        {
            var passHash = pass.MD5EncodePassword();
            var custInfo = CheckUserInfo(Username, passHash);
            return custInfo;
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            refCode = ((int)ErrorCodeDetail.Exception).ErrorCodeFormat();
            refMesg = _distributeCache.GetErrCodeMessage((int)ErrorCodeDetail.Exception,
                Channel.Back.ToEnumDescription(), Language.Vi.ToEnumDescription());
            return null;
        }
    }

    private UserRoleDto CheckUserInfo(string Username, string passHash)
    {
        try
        {
            var info = _dbContext.Users.FirstOrDefault(m => m.Username == Username && m.Password == passHash);

            if (info == null)
                return null;

            return new UserRoleDto
            {
                Username = info.Username,
                Phone = info.Phone,
                Email = info.Email
            };
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return null;
        }
    }

    #region helper methods

    private bool RevokeToken(string accessToken)
    {
        var key = CacheKeys.TokenRevoked + accessToken;
        var timeout = GetTokenLifeTime((int)TokenType.AccessToken);
        return _redisCache.SetKey(key, true, timeout);
    }

    private bool SetTokenDevice(string accessToken, string Username)
    {
        var pattern = CacheKeys.TokenCurrDevice + Username;
        _redisCache.RemoveKeyByPattern(pattern);

        var key = pattern + accessToken;
        var timeout = GetTokenLifeTime((int)TokenType.AccessToken);
        return _redisCache.SetKey(key, true, timeout);
    }

    private string GenerateAccessToken(UserDto user, DetectionInfo detectionInfo, ref DateTime expiresTime)
    {
        try
        {
            var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Secret"]));
            var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

            Claim[] claims;
            if (_onlyDevice)
                claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, user.Username),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Iss, _config["Jwt:Issuer"]),
                    new Claim(JwtRegisteredClaimNames.Aud, _config["Jwt:Audience"]),
                    new Claim("Device", detectionInfo.Device),
                    new Claim("Browser", detectionInfo.Browser),
                    new Claim("Platform", detectionInfo.Platform),
                    new Claim("Engine", detectionInfo.Engine),
                    new Claim("ClientIp", detectionInfo.ClientIp)
                };
            else
                claims = new[]
                {
                    new Claim(JwtRegisteredClaimNames.Sub, user.Username),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                    new Claim(JwtRegisteredClaimNames.Iss, _config["Jwt:Issuer"]),
                    new Claim(JwtRegisteredClaimNames.Aud, _config["Jwt:Audience"])
                };

            var timeout = GetTokenLifeTime((int)TokenType.AccessToken);
            expiresTime = DateTime.Now.AddMinutes(timeout);
            var token = new JwtSecurityToken(
                _config["Jwt:Issuer"],
                _config["Jwt:Audience"],
                claims,
                expires: expiresTime,
                signingCredentials: credentials
            );

            var accessToken = new JwtSecurityTokenHandler().WriteToken(token);

            if (_onlyDevice) _ = SetTokenDevice(accessToken, user.Username);

            return accessToken;
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return string.Empty;
        }
    }

    private RefreshToken GenerateRefreshToken(string Username, string ipAddress)
    {
        try
        {
            using (var rngCryptoServiceProvider = new RNGCryptoServiceProvider())
            {
                var randomBytes = new byte[64];
                rngCryptoServiceProvider.GetBytes(randomBytes);

                return new RefreshToken
                {
                    Token = Convert.ToBase64String(randomBytes),
                    Expires = DateTime.Now.AddHours(GetTokenLifeTime((int)TokenType.RefreshToken)),
                    CreatedDate = DateTime.Now,
                    CreatedByIp = ipAddress,
                    Username = Username
                };
            }
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            return null;
        }
    }

    public int GetTokenLifeTime(int tokenType)
    {
        var tokenLifeTime = 0;
        switch (tokenType)
        {
            case (int)TokenType.AccessToken:
                int.TryParse(_config["Jwt:AccessLifeTime"], out tokenLifeTime);
                _ = _ = tokenLifeTime == 0 ? CommonConstants._15Minutes : tokenLifeTime;
                break;
            case (int)TokenType.RefreshToken:
                int.TryParse(_config["Jwt:RefreshLifeTime"], out tokenLifeTime);
                _ = _ = tokenLifeTime == 0 ? CommonConstants._1Hours : tokenLifeTime;
                break;
        }

        return tokenLifeTime;
    }

    private static LogLogin BuildLogLogin(string Username, string channel, DetectionInfo detectInfo)
    {
        return new LogLogin
        {
            Username = Username,
            Channel = channel,
            IpAddress = detectInfo.ClientIp,
            Device = detectInfo.Device,
            Browser = detectInfo.Browser,
            Platform = detectInfo.Platform,
            Engine = detectInfo.Engine,
            LoginTime = DateTime.Now
        };
    }

    #endregion helper methods
}