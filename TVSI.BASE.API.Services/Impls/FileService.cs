﻿using TVSI.BASE.API.Models.Entities.Demo;

namespace TVSI.BASE.API.Services.Impls
{
    public class FileService : BaseService<FileService>, IFileService
    {
        public FileService(ILogger<FileService> logger, DemoDbContext dbContext, IConfiguration config)
            : base(logger, config, dbContext)
        {
        }

        public void ExportDataToFileTemplate(string fileExportPath, string templateFilePath, DataSet data, string[] deliminator, FileType fileType = FileType.Excel)
        {
            switch (fileType)
            {
                case FileType.Excel:
                    ExportDataToExcelTemplate(fileExportPath, templateFilePath, data, deliminator);
                    break;
                case FileType.Doc:
                case FileType.Docx:
                    ExportDataToDocTemplate(fileExportPath, templateFilePath, data, deliminator);
                    break;
                case FileType.PDF:
                case FileType.Xml:
                case FileType.Html:
                case FileType.Text:
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(fileType), fileType, null);
            }
        }

        public void ExportErrorMessageToFile(string fileExportPath, object noti, FileType fileType = FileType.Excel) 
        {
            try
            {
                var excel = new ExcelPackage();

                var workSheet = excel.Workbook.Worksheets.Add("Sheet1");

                workSheet.TabColor = Color.Red;

                workSheet.Row(1).Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                workSheet.Row(1).Style.Font.Bold = true;

                workSheet.Cells[1, 1].Value = JsonConvert.SerializeObject(noti);

                if (File.Exists(fileExportPath))
                    File.Delete(fileExportPath);

                var objFileStream = File.Create(fileExportPath);
                objFileStream.Close();

                File.WriteAllBytes(fileExportPath, excel.GetAsByteArray());
                excel.Dispose();
            }
            catch (Exception ex)
            {
                _logger.LogInformation($"{ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            }
        }

        #region Helper methods

        public void DeleteFile(string directoryPath, string wildcard)
        {
            var dir = new DirectoryInfo(directoryPath);

            foreach (var file in dir.EnumerateFiles(wildcard)) file.Delete();
        }

        #endregion Helper methods

        #region Export File Html

        public void ExportHtmlFromTemplate(string filename, string templateFilename, DataSet data, string[] deliminator)
        {
        }

        #endregion Export File Html
        
        #region Export File Excel

        private void ExportDataToExcelTemplate(string filename, string templateFilename, DataSet data, string[] deliminator)
        {
            if (File.Exists(filename))
                File.Delete(filename);

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
            using (var file = new FileStream(filename, FileMode.CreateNew))
            {
                using (var temp = new FileStream(templateFilename, FileMode.Open, FileAccess.Read))
                {
                    using (var xls = new ExcelPackage(file, temp))
                    {
                        foreach (var n in xls.Workbook.Names) FillDataToWorksheetWithExcelNameRange(data, n.Worksheet, n, deliminator);

                        foreach (var ws in xls.Workbook.Worksheets)
                        foreach (var n in ws.Names)
                            FillDataToWorksheetWithExcelNameRange(data, ws, n, deliminator);

                        foreach (var ws in xls.Workbook.Worksheets) FillDataToWorksheet(data, ws, deliminator);

                        xls.Save();
                    }
                }
            }
        }

        private void FillDataToWorksheet(DataSet data, ExcelWorksheet ws, string[] deliminator)
        {
            try
            {
                foreach (var c in ws.Cells)
                {
                    var s = "" + c.Value;
                    if (s.StartsWith(deliminator[0]) == false &&
                        s.EndsWith(deliminator[1]) == false)
                        continue;
                    s = s.Replace(deliminator[0], "").Replace(deliminator[1], "");
                    var ss = s.Split('.');
                    try
                    {
                        if (data.Tables[ss[0]] != null) c.Value = data.Tables[ss[0]]?.Rows[0][ss[1]];
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError($"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            }
        }

        private void FillDataToWorksheetWithExcelNameRange(DataSet data, ExcelWorksheet ws, ExcelNamedRange n, string[] deliminator)
        {
            try
            {
                if (data.Tables.Contains(n.Name) == false)
                    return;

                var dt = data.Tables[n.Name];

                var row = n.Start.Row;
                if (dt == null) return;

                ws.InsertRow(row + 1, dt.Rows.Count - 1, row);

                var colName = new string?[n.Columns];
                var colStyle = new int[n.Columns];
                for (var i = 0; i < n.Columns; i++)
                {
                    var val = (n.Value as object[,])?[0, i] ?? string.Empty;

                    colName[i] = val.ToString()?.Replace(deliminator[0], "").Replace(deliminator[1], "");
                    if (colName[i]!.Contains("."))
                        colName[i] = colName[i]?.Split('.')[1];
                    colStyle[i] = ws.Cells[row, n.Start.Column + i].StyleID;
                }

                if (dt.Rows.Count > 0)
                    foreach (DataRow r in dt.Rows)
                    {
                        for (var col = 0; col < n.Columns; col++)
                        {
                            if (dt.Columns.Contains(colName[col]!))
                                ws.Cells[row, n.Start.Column + col].Value = r[colName[col]!];
                            ws.Cells[row, n.Start.Column + col].StyleID = colStyle[col];
                        }

                        row++;
                    }
                else
                    for (var col = 0; col < n.Columns; col++)
                    {
                        if (dt.Columns.Contains(colName[col]!))
                            ws.Cells[row, n.Start.Column + col].Value = "";
                        ws.Cells[row, n.Start.Column + col].StyleID = colStyle[col];
                    }

                // extend table formatting range to all rows
                foreach (var t in ws.Tables)
                {
                    var a = t.Address;
                    if (n.Start.Row.Between(a.Start.Row, a.End.Row) &&
                        n.Start.Column.Between(a.Start.Column, a.End.Column))
                        ExtendRows(t, dt.Rows.Count - 1);
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            }
        }

        public void ExtendRows(ExcelTable excelTable, int count)
        {
            var ad = new ExcelAddress(excelTable.Address.Start.Row,
                excelTable.Address.Start.Column,
                excelTable.Address.End.Row + count,
                excelTable.Address.End.Column);
        }

        #endregion Export File Excel

        #region Export File Doc

        public void ExportDataToDocTemplate(string fileExportPath, string templateFilePath, DataSet data, string[] deliminator)
        {
            #region Using OpenXml

            var xData = data.Tables[1].ToJsonObject();
            var arrJson = JArray.Parse(xData);
            var dict = new Dictionary<string, string>();
            foreach (var item in arrJson)
            {
                dict = item.ToString().ToDictionary();
                break;
            }

            var byteArray = File.ReadAllBytes(templateFilePath);
            using var stream = new MemoryStream();
            stream.Write(byteArray, 0, (int)byteArray.Length);
            using (var wordDoc = WordprocessingDocument.Open(stream, true))
            {
                // Modify the document as necessary.
                var doc = wordDoc.MainDocumentPart?.Document;
                var body = doc?.Body;
                var paras = body?.Elements<Paragraph>();

                if (paras != null)
                {
                    foreach (var para in paras)
                    {
                        foreach (var run in para.Elements<Run>())
                        {
                            foreach (var text in run.Elements<Text>())
                            {
                                foreach (var (key, value) in dict)
                                {
                                    if (text.Text.Contains($"{deliminator[0]}{key}{deliminator[1]}"))
                                    {
                                        text.Text = text.Text.Replace($"{deliminator[0]}{key}{deliminator[1]}", value);
                                    }
                                }
                            }
                        }
                    }
                }

                var tables = doc?.Descendants<Table>().ToList();
                if (tables != null)
                {
                    foreach (var text in from t in tables select t.Elements<TableRow>() into rows from row in rows select row.Elements<TableCell>() into cells from cell in cells from para in cell.Elements<Paragraph>() from run in para.Elements<Run>() from text in run.Elements<Text>() select text)
                    {
                        foreach (var (key, value) in dict)
                        {
                            if (text.Text.Contains($"{deliminator[0]}{key}{deliminator[1]}"))
                            {
                                text.Text = text.Text.Replace($"{deliminator[0]}{key}{deliminator[1]}", value);
                            }
                        }
                    }
                }

                //Make changes to the document and save it.
                wordDoc.MainDocumentPart?.Document.Save();
                wordDoc.Close();
            }
            // Save the file with the new name
            stream.Position = 0;
            File.WriteAllBytes(fileExportPath, stream.ToArray());

            #endregion Using OpenXml

        }

        #endregion Export File Doc
    }

    public static class IntBetween
    {
        public static bool Between(this int v, int a, int b)
        {
            return v >= a && v <= b;
        }
    }
}