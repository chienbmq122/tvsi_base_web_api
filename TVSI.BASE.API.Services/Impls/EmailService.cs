﻿using TVSI.BASE.API.Models.Entities.Demo;

namespace TVSI.BASE.API.Services.Impls
{
    public class EmailService : BaseService<EmailService>, IEmailService
    {
        public EmailService(ILogger<EmailService> logger, DemoDbContext dbContext, IConfiguration config)
            : base(logger, config, dbContext)
        {
        }
    }
}