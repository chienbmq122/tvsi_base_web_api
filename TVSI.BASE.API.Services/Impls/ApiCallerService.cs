﻿using System.Diagnostics;
using System.Net;
using OfficeOpenXml.Style.XmlAccess;
using TVSI.BASE.API.Models.Entities.Demo;

namespace TVSI.BASE.API.Services.Impls;

public class ApiCallerService : BaseService<ApiCallerService>, IApiCallerService
{
    private readonly int _timeout;

    public ApiCallerService(ILogger<ApiCallerService> logger, DemoDbContext dbContext, IConfiguration config)
        : base(logger, config, dbContext)
    {
        int.TryParse(_config["Timeout:ApiCaller"], out _timeout);
    }

    public async Task<Tuple<int, string, string>?> ApiCaller(string url, string postData, ApiMethod method,
        ApiContentType contentType = ApiContentType.Json, ApiCaller apiCaller = Models.Enums.ApiCaller.HttpClient)
    {
        var stopWatch = new Stopwatch();
        stopWatch.Start();
        Tuple<int, string, string>? result;
        try
        {
            if (string.IsNullOrEmpty(postData)) postData = "{}";

            _logger.LogInformation(
                $"{MethodBase.GetCurrentMethod()?.Name}: {url}, method: {method}, contentType: {contentType}, data: {postData.WriteLogNonePassword()}");

            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 |
                                                   SecurityProtocolType.Tls12 |
                                                   SecurityProtocolType.Tls13 | /*SecurityProtocolType.Ssl3 |*/
                                                   (SecurityProtocolType)3072;
            result = apiCaller switch
            {
                Models.Enums.ApiCaller.HttpClient => await CallByHttpClientAsync(url, method, contentType, postData),
                Models.Enums.ApiCaller.HttpWebRequest => CallByHttpWebRequest(url, method, contentType, postData),
                _ => new Tuple<int, string, string>(-1, "Phương thức Caller không được hỗ trợ.", string.Empty)
            };
        }
        catch (Exception ex)
        {
            _logger.LogError(
                $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            result = new Tuple<int, string, string>(-1, $"Exception: {ex.Message}", string.Empty);
        }
        finally
        {
            stopWatch.Stop();
            var ts = stopWatch.Elapsed.TotalMilliseconds;

            _logger.LogInformation($"{MethodBase.GetCurrentMethod()?.Name}: {url}, Response time: {ts}");
        }

        return result;
    }

    #region HttpWebRequest

    private Tuple<int, string, string>? CallByHttpWebRequest(string url, ApiMethod method, ApiContentType contentType,
        string postData)
    {
        var resData = string.Empty;
        var resCode = -1;
        var resMesg = "Failed";

        try
        {
            var httpWebRequest = (HttpWebRequest)WebRequest.Create(url);
            httpWebRequest.Method = method.ToEnumDescription();
            httpWebRequest.ContentType = contentType.ToEnumDescription();
            httpWebRequest.Timeout = _timeout == 0 ? CommonConstants.ApiCallerTimeout : _timeout;

            if (!string.IsNullOrWhiteSpace(postData))
                using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
                {
                    streamWriter.Write(postData);
                }
            else
                httpWebRequest.ContentLength = 0;

            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var data = httpResponse.GetResponseStream())
            {
                using (var streamReader = new StreamReader(data))
                {
                    resData = streamReader.ReadToEnd();
                }
            }

            _logger.LogInformation($"{MethodBase.GetCurrentMethod()?.Name}: {url} Result: {resData}");

            resCode = 1;
            resMesg = "Success";
        }
        catch (WebException wex)
        {
            _logger.LogError($"{MethodBase.GetCurrentMethod()?.Name}: {url} has been webException: {wex}");
            try
            {
                using (var response = wex.Response)
                {
                    var httpResponse = (HttpWebResponse)response!;
                    using (var data = httpResponse.GetResponseStream())
                    {
                        using (var streamReader = new StreamReader(data))
                        {
                            resData = streamReader.ReadToEnd();
                        }
                    }
                }

                _logger.LogError(
                    $"{MethodBase.GetCurrentMethod()?.Name} WebException GetResponseStream: {url} Result: {resData}");
                resMesg = $"WebException: {wex.InnerException}";
            }
            catch (Exception ex)
            {
                _logger.LogError(
                    $"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                resMesg = $"WebException - Exception: {ex.Message}";
            }
        }

        return new Tuple<int, string, string>(resCode, resMesg, resData);
    }

    #endregion HttpWebRequest

    #region HttpClient

    public async Task<Tuple<int, string, string>?> CallByHttpClientAsync(string url, ApiMethod method,
        ApiContentType contentType, string postData)
    {
        return method switch
        {
            ApiMethod.GET => await Get(url, contentType, postData),
            ApiMethod.POST => await Post(url, contentType, postData),
            ApiMethod.PUT => await Put(url, contentType, postData),
            ApiMethod.DELETE => await Delete(url, contentType, postData),
            _ => throw new ArgumentOutOfRangeException()
        };
    }

    private static async Task<Tuple<int, string, string>?> Post(string url, ApiContentType contentType, string postData)
    {
        var resData = string.Empty;
        var resCode = -1;
        var resMesg = "Failed";
        try
        {
            using var client = new HttpClient();
            var content = new StringContent(postData, Encoding.UTF8, contentType.ToEnumDescription());
            var result = await client.PostAsync(url, content);
            if (result.IsSuccessStatusCode)
            {
                resCode = 1;
                resMesg = "Success";
            }

            resData = await result.Content.ReadAsStringAsync();
        }
        catch (Exception ex)
        {
            resCode = -1;
            resMesg = ex.Message;
        }

        return new Tuple<int, string, string>(resCode, resMesg, resData);
    }

    private static async Task<Tuple<int, string, string>?> Put(string url, ApiContentType contentType, string postData)
    {
        var resData = string.Empty;
        var resCode = -1;
        var resMesg = "Failed";
        try
        {
            using var client = new HttpClient();
            var content = new StringContent(postData, Encoding.UTF8, contentType.ToEnumDescription());
            var result = await client.PutAsync(url, content);
            if (result.IsSuccessStatusCode)
            {
                resCode = 1;
                resMesg = "Success";
            }

            resData = await result.Content.ReadAsStringAsync();
        }
        catch (Exception ex)
        {
            resCode = -1;
            resMesg = ex.Message;
        }

        return new Tuple<int, string, string>(resCode, resMesg, resData);
    }

    private static async Task<Tuple<int, string, string>?> Get(string url, ApiContentType contentType, string postData)
    {
        var resData = string.Empty;
        var resCode = -1;
        var resMesg = "Failed";
        try
        {
            using var client = new HttpClient();
            var dic = JsonConvert.DeserializeObject<Dictionary<string, object>>(postData);
            if (dic is { Count: > 0 })
            {
                var i = 0;
                foreach (var entry in dic)
                {
                    if (i == 0)
                        url += $"?{entry.Key}={entry.Value}";
                    else
                        url += $"&{entry.Key}={entry.Value}";

                    i++;
                }
            }

            var result = await client.GetAsync(url);
            if (result.IsSuccessStatusCode)
            {
                resCode = 1;
                resMesg = "Success";
            }

            resData = await result.Content.ReadAsStringAsync();
        }
        catch (Exception ex)
        {
            resCode = -1;
            resMesg = ex.Message;
        }

        return new Tuple<int, string, string>(resCode, resMesg, resData);
    }

    private static async Task<Tuple<int, string, string>?> Delete(string url, ApiContentType contentType,
        string postData)
    {
        var resData = string.Empty;
        var resCode = -1;
        var resMesg = "Failed";
        try
        {
            using var client = new HttpClient();
            var content = new StringContent(postData, Encoding.UTF8, contentType.ToEnumDescription());
            var result = await client.DeleteAsync(url);
            if (result.IsSuccessStatusCode)
            {
                resCode = 1;
                resMesg = "Success";
            }

            resData = await result.Content.ReadAsStringAsync();
        }
        catch (Exception ex)
        {
            resCode = -1;
            resMesg = ex.Message;
        }

        return new Tuple<int, string, string>(resCode, resMesg, resData);
    }

    #endregion HttpClient
}