﻿using TVSI.BASE.API.Models.Entities.Demo;

namespace TVSI.BASE.API.Services.Impls
{
    public class RedisService : BaseService<RedisService>, IRedisService
    {
        private readonly IConnectionMultiplexer _connectionMultiplexer;
        private readonly IDatabase _db;
        private readonly IDistributedCache _distributedCache;

        public RedisService(IDistributedCache distributedCache, IConnectionMultiplexer connectionMultiplexer, 
            ILogger<RedisService> logger, DemoDbContext dbContext, IConfiguration config)
            : base(logger, config, dbContext)
        {
            _distributedCache = distributedCache;
            _connectionMultiplexer = connectionMultiplexer;
            var redisDb = _config["RedisQueue:Database"] == null ? 0 : int.Parse(_config["RedisQueue:Database"]);
            _db = _connectionMultiplexer.GetDatabase(redisDb);
        }

        #region Redis string

        public T GetKey<T>(string key)
        {
            try
            {
                var data = _distributedCache.Get(key);
                if (data == null)
                    return default!;

                var serialize = Encoding.UTF8.GetString(data);
                var result = JsonConvert.DeserializeObject<T>(serialize);
                return result ?? default!;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return default!;
            }
        }

        public bool SetKey(string key, object value, int expireMinutes = 0)
        {
            try
            {
                var serialize = JsonConvert.SerializeObject(value);
                var data = Encoding.UTF8.GetBytes(serialize);

                var expTime = expireMinutes == 0
                    ? new DistributedCacheEntryOptions()
                    : new DistributedCacheEntryOptions
                    {
                        AbsoluteExpiration = DateTimeOffset.Now.AddMinutes(expireMinutes),
                        SlidingExpiration = TimeSpan.FromMinutes(expireMinutes - expireMinutes / 2)
                    };

                _distributedCache.Set(key, data, expTime);
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return false;
            }
        }

        public bool RemoveKey(string key)
        {
            try
            {
                if (string.IsNullOrEmpty(key))
                    return true;
                _distributedCache.Remove(key);
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return false;
            }
        }
        #endregion Redis string

        #region Multiplexer
        public List<string> GetAllKeys(int database)
        {
            var result = new List<string>();
            try
            {
                foreach (var ep in _connectionMultiplexer.GetEndPoints())
                {
                    var server = _connectionMultiplexer.GetServer(ep);
                    var keys = server.Keys(database);
                    result.AddRange(keys.Select(key => key.ToString()));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            }

            return result;
        }

        public List<string> GetAllKeysByPattern(int database, string pattern)
        {
            var result = new List<string>();
            try
            {
                foreach (var ep in _connectionMultiplexer.GetEndPoints())
                {
                    var server = _connectionMultiplexer.GetServer(ep);
                    var keys = server.Keys(database, pattern + "*");
                    result.AddRange(keys.Select(key => key.ToString()));
                }
            }
            catch (Exception ex)
            {
                _logger.LogError($"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
            }

            return result;
        }

        public bool RemoveKeyByPattern(string pattern)
        {
            try
            {
                if (string.IsNullOrWhiteSpace(pattern))
                    return false;

                foreach (var ep in _connectionMultiplexer.GetEndPoints())
                {
                    var server = _connectionMultiplexer.GetServer(ep);
                    var keys = server.Keys(pattern: pattern + "*");
                    foreach (var key in keys) _distributedCache.Remove(key);
                }

                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return false;
            }
        }
        #endregion Multiplexer

        #region Redis hash

        public bool HashSet<T>(string key, T value)
        {
            try
            {
                var hashEntries = ToHashEntries(value);
                _db.HashSet(key, hashEntries);
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return false;
            }
        }

        public bool HashSet(string key, string hashKey, string value)
        {
            try
            {
                _db.HashSet(key, hashKey, value);
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return false;
            }
        }

        public bool HashExists(string key, string hashKey)
        {
            try
            {
                return _db.HashExists(key, hashKey);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return false;
            }
        }

        public HashEntry[]? HashGetAll(string key)
        {
            try
            {
                var hashEntries = _db.HashGetAll(key);
                return hashEntries;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return default;
            }
        }

        public List<T>? HashGetAll<T>(string key)
        {
            try
            {
                var hashEntries = _db.HashGetAll(key);
                return ConvertFromRedisHash<List<T>>(hashEntries);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return default;
            }
        }

        public RedisValue[]? HashValues(string key)
        {
            try
            {
                var redisValue = _db.HashValues(key);
                return !redisValue.Any() ? null : redisValue;
            }
            catch (Exception ex)
            {
                _logger.LogError($"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return null;
            }
        }

        public RedisValue HashGet(string key, string hashKey)
        {
            try
            {
                var redisValue = _db.HashGet(key, hashKey);
                return redisValue.HasValue ? redisValue : new RedisValue();
            }
            catch (Exception ex)
            {
                _logger.LogError($"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return new RedisValue();
            }
        }

        public bool HashRemove(string key, string hashKey)
        {
            try
            {
                return !_db.HashExists(key, hashKey) || _db.HashDelete(key, hashKey);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{MethodBase.GetCurrentMethod()?.Name}: {ex.Message} - {ex.InnerException} - {ex.StackTrace}");
                return false;
            }
        }

        #endregion Redis hash

        #region Helper method

        private static HashEntry[] ToHashEntries(object obj)
        {
            var properties = obj.GetType().GetProperties();
            return properties
                .Where(x => x.GetValue(obj) != null)
                .Select(p =>
                {
                    var propertyValue = p.GetValue(obj);
                    var hashValue = propertyValue is IEnumerable<object> ? JsonConvert.SerializeObject(propertyValue) : propertyValue?.ToString();
                    return new HashEntry(p.Name, hashValue);
                })
                .ToArray();
        }

        private static T? ConvertFromRedisHash<T>(HashEntry[] hashEntries)
        {
            var properties = typeof(T).GetProperties();
            var obj = Activator.CreateInstance(typeof(T));
            foreach (var property in properties)
            {
                var entry = hashEntries.FirstOrDefault(g => g.Name.ToString().Equals(property.Name));
                if (entry.Equals(new HashEntry())) continue;
                property.SetValue(obj, Convert.ChangeType(entry.Value.ToString(), property.PropertyType));
            }

            return obj == null ? default : (T)obj;
        }

        #endregion Helper method
    }
}